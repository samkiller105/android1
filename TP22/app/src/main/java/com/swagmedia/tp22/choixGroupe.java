package com.swagmedia.tp22;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.*;

import android.widget.AdapterView.*;
import android.widget.RadioGroup;
import android.widget.Toast;

/**
 * auteur : Samuel Fortin - 1341526
 *
 *  Page principale servant à instancier un titre, des radios buttons et une listeview. C'est un lien vers les autres pages
 * */
public class ChoixGroupe extends AppCompatActivity {


    ArrayList<String> listItems;
    ArrayList<String> liensWebs;
    ArrayList<String> liensYoutube;
    ArrayAdapter<String> adapter;
    Intent nouvellePage;
    RadioGroup radiogroup;

    /**
     * A faire lors de la création, setup les éléments de l'interface comme demandé
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choix_groupe);

        // instance utilisable des radios buttons
        radiogroup = (RadioGroup) findViewById(R.id.radioGroup);

        gererListView();


    }

    /**
     * Instanciation de la liste view
     * */
    public void gererListView()
    {

        // ajout dans la list view
        ajouterListItems(new String[]{"Tool", "Porcupine Tree", "Radiohead"});

        // ajout dans la liste de liens webs
        ajouterListLiens(new String[]{"http://www.toolband.com", "http://www.porcupinetree.com/", "http://www.radiohead.fr/"});

        // ajout des liens youtube dans la liste
        ajouterLiensYoutube(new String[]{"http://www.youtube.com/watch?v=R2F_hGwD26g", "http://www.youtube.com/watch?v=0UHwkfhwjsk", "http://www.youtube.com/watch?v=TTAU7lLDZYU"});

        //Adapter nécéssaire
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listItems);

        //listview de notre interface
        ListView lv = (ListView) findViewById(R.id.listViewChoixGroupe);
        lv.setAdapter(adapter);

        //ecouteurs des actions listview
        lv.setOnItemClickListener(new OnItemClickListener() {

            public void onItemClick(AdapterView<?> parentAdapter, View view, int position,
                                    long id) {

                int boutonSelectionne = radiogroup.getCheckedRadioButtonId();
                View radioButton = radiogroup.findViewById(boutonSelectionne);
                int idx = radiogroup.indexOfChild(radioButton);


                if (idx == 0) {
                    nouvellePage = new Intent(Intent.ACTION_VIEW, Uri.parse(liensYoutube.get(position)));

                } else if (idx == 1) {
                    nouvellePage = new Intent(getApplicationContext(), Images.class);

                } else if (idx == 2) {

                    nouvellePage = new Intent(Intent.ACTION_VIEW, Uri.parse(liensWebs.get(position)));
                }

                String nomGroupe = listItems.get(position);
                nouvellePage.putExtra("EXTRA_CHOIX_NOM", nomGroupe);
                nouvellePage.putExtra("EXTRA_CHOIX_ID", position);
                startActivity(nouvellePage);

            }
        });
    }

    /**
     * Methode servant a afficher du code dans la console
     *
     * Paramètre = object message
     */
    public static void out(Object msg) {
        Log.i("info", msg.toString());
    }


    /**
     * Ajoute les items demandées dans la liste des items servant a la liste view.
     *
     * Param : tableau de string
     */
    public void ajouterListItems(String[] groupe) {


        if (listItems == null) {
            listItems = new ArrayList<String>();
        }
        for (int i = 0; i < groupe.length; i++) {

            listItems.add(groupe[i]);
        }


    }


    public ArrayList<String> getListItems() {

        return this.listItems;

    }

    /**
     * Ajoute les items demandées dans la liste des items servant aux liens.
     *
     * Param : tableau de string
     * */
    public void ajouterListLiens(String[] liens) {

        if (liensWebs == null) {
            liensWebs = new ArrayList<String>();
        }

        for (int i = 0; i < liens.length; i++) {

            liensWebs.add(liens[i]);
        }

    }

    public ArrayList<String> getListLiens() {

        return this.liensWebs;

    }

    /**
     * Ajoute les items demandées dans la liste des items servant aux liens des videos.
     *
     * Param : tableau de string
     * */
    public void ajouterLiensYoutube(String[] liens) {

        if (liensYoutube == null) {
            liensYoutube = new ArrayList<String>();
        }

        for (int i = 0; i < liens.length; i++) {

            liensYoutube.add(liens[i]);
        }

    }
    public void afficherTextePopUp(String text) {

        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();

    }


}
