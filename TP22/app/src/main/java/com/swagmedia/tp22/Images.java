package com.swagmedia.tp22;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.TextView;
import android.widget.ViewFlipper;

/**
 * auteur : Samuel Fortin - 1341526
 *  Page dynamique qui affiche les images et qui gère les transitions
 * */
public class Images extends AppCompatActivity {


    ViewFlipper viewFlipper;
    private float lastX;
    private TextView textv;

    /**
     * Constructeur de la classe qui détermine laquelle des view à instancier
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        String nomGroupe = extras.getString("EXTRA_CHOIX_NOM");
        int idGroupe = extras.getInt("EXTRA_CHOIX_ID");

        if (idGroupe == 0)
        {
            setContentView(R.layout.activity_images);
        }
        else if (idGroupe == 1)
        {
            setContentView(R.layout.activity_images_porc);
        }
        else if (idGroupe == 2)
        {
            setContentView(R.layout.activity_images_radiohead);
        }

        else
        {
            setContentView(R.layout.activity_images);
        }

        textv = (TextView) findViewById(R.id.nomGroupe);
        textv.setText(nomGroupe);
        viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper);

    }


    /**
     * Actions lorsqu'on touche les images
     * */
    public boolean onTouchEvent(MotionEvent touchevent) {

        switch (touchevent.getAction()) {
            // when user first touches the screen to swap
            case MotionEvent.ACTION_DOWN: {
                lastX = touchevent.getX();
                break;
            }
            case MotionEvent.ACTION_UP: {
                float currentX = touchevent.getX();

                // if left to right swipe on screen
                if (lastX < currentX) {
                    // If no more View/Child to flip
                    if (viewFlipper.getDisplayedChild() == 0)
                        break;

                    // set the required Animation type to ViewFlipper
                    // The Next screen will come in form Left and current Screen will go OUT from Right
                    viewFlipper.setInAnimation(this, R.anim.in_from_left);
                    viewFlipper.setOutAnimation(this, R.anim.out_to_right);
                    // Show the next Screen
                    viewFlipper.showPrevious();
                }

                // if right to left swipe on screen
                if (lastX > currentX) {
                    if (viewFlipper.getDisplayedChild() == 1)
                        break;

                    // set the required Animation type to ViewFlipper
                    // The Next screen will come in form Right and current Screen will go OUT from Left
                    viewFlipper.setInAnimation(this, R.anim.in_from_right);
                    viewFlipper.setOutAnimation(this, R.anim.out_to_left);
                    // Show The Previous Screen
                    viewFlipper.showPrevious();
                }
                break;
            }
        }
        return false;
    }


}
